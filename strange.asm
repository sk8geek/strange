; Radiator Cover Display
; Version 0.3
; December 2005
; (C) Copyright 1998-2005 Steven J Lilley
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation; either version 2 of the License, or
; (at your option) any later version.
; 
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
; GNU General Public License for more details.
; 
; You should have received a copy of the GNU General Public License
; along with this program; if not, write to the Free Software
; Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
; MA    02111-1307    USA
;

; Define processor/radix

    list    p=16F84, r=HEX
    errorlevel -302

; Define CONSTANTS
;

; **** System Registers ****
; (Bank 0)

    constant INDF=0x00      ; Indirect address reference, refers to register held in FSR
    constant STATUS=0x03    ; Status Register
    constant FSR=0x04       ; Indirect address pointer
    constant PORTA=0x05     ; Port A register
    constant PORTB=0x06     ; Port B register
    constant EEDATA=0x08    ; EEPROM data register
    constant EEADR=0x09     ; EEPROM address register
    constant INTCON=0x0B    ; Interrupt control register
    
; (Bank 1)

    constant OPTREG=0x81    ; Option register
    constant TRISA=0x85     ; Port A data direction register
    constant TRISB=0x86     ; Port B data direction register
    constant EECON1=0x88    ; EEPROM control register
    constant EECON2=0x89    ; EEPROM control register

; ****  User Registers  ****

; General purpose
TMP         equ     0x0C
TMP2        equ     0x0D
BYTE1       equ     0x0E
BYTE2       equ     0x0F

; Timer
SUBS        equ     0x10
SECS        equ     0x11
TENS        equ     0x12
MINS        equ     0x13
HOURS       equ     0x14

; Control
FLAGS       equ     0x15
ENABLES     equ     0x16
MODE        equ     0x17
MODELOCK    equ     0x18
STATEKEEP   equ     0x19
FSRKEEPER   equ     0x1A
LIGHTSON    equ     0x1B

; Cylon
CYLONA      equ     0x20
CYLONB      equ     0x21
CYLONCTL    equ     0x22
CYLONLEVEL  equ     0x23
CYLONMODE   equ     0x24
CYLONSCALE  equ     0x25

; Cat Eyes
; ****  DO NOT ALTER REGISTER ORDER OF THESE SIX  ****
CAT1COLOR   equ     0x28
CAT1MODE    equ     0x29
CAT2COLOR   equ     0x2A
CAT2MODE    equ     0x2B
CAT3COLOR   equ     0x2C
CAT3MODE    equ     0x2D
CAT_TMP     equ     0x2E


; Fade control working registers
W1R         equ     0x30
W2R         equ     0x31
W3R         equ     0x32
W1G         equ     0x33
W2G         equ     0x34
W3G         equ     0x35
WCL         equ     0x36
WCS         equ     0x37


; ****  Bit references  ****
SECflag     equ     0x0
MINflag     equ     0x1
TENflag     equ     0x2
BEEPflag    equ     0x3
CYLONcarry  equ     0x4

; PORTA
Latch       equ     0x0
Clock       equ     0x1
SerialData  equ     0x2
OutEnable   equ     0x3

; STATUS register
REGBANK     equ     0x5     ; The bank indicator bit
WATCHDOG    equ     0x4     ; Watchdog timeout 
ZEROFLAG    equ     0x2     ; The ZERO flag bit
DIGITCARRY  equ     0x1     ; The DIGIT CARRY bit
CARRYBIT    equ     0x0     ; The CARRY bit

; OPTION register
PRESCALER   equ     0x3     ; Prescaler assignment

; INTCON register
GIE         equ     0x7     ; Global Interrupt Enable bit
EEIEN       equ     0x6
T0IEN       equ     0x5
INTEN       equ     0x4
RBIEN       equ     0x3
TIMER       equ     0x2
INTERRUPT   equ     0x1
RBIFLG      equ     0x0

; PORTB register
LightsOff   equ     0x0
Cat1Red     equ     0x1
Cat1Green   equ     0x2
Cat2Red     equ     0x3
Cat2Green   equ     0x4
Cat3Red     equ     0x5
Cat3Green   equ     0x6
Sounder     equ     0x7

; CATMODE register
Cat1IncGrn  equ     0x0
Cat1IncRed  equ     0x1
Cat2IncGrn  equ     0x2
Cat2IncRed  equ     0x3
Cat3IncGrn  equ     0x4
Cat3IncRed  equ     0x5

; CATxMODE register
RedRate1    equ     0x0
RedRate2    equ     0x1
RedSign     equ     0x2
RedBounce   equ     0x3
GrnRate1    equ     0x4
GrnRate2    equ     0x5
GrnSign     equ     0x6
GrnBounce   equ     0x7

; register references
WREG        equ     0x0
SELF        equ     0x1

; **** Macros ****

ENABLE_INTERRUPTS macro
    bsf     INTCON,GIE
    endm

DISABLE_INTERRUPTS macro
    bcf     INTCON,GIE
    btfsc   INTCON,GIE
    endm

; ***************
; **   START   **
; ***************
Reset:      org 0x000
    call    Initialize
Standby:
    sleep
    nop
Int:        org 0x004
    DISABLE_INTERRUPTS
    goto    Int                     ; Test which INT woke us up
    btfss   STATUS, WATCHDOG        ; Watchdog?
    goto    Standby                 ;   then go back to sleep
    btfsc   INTCON, TIMER           ; Timer overflowed?
    call    SubSecondJobs           ;   then increment our timer
    btfsc   INTCON, INTERRUPT       ; Light level change?
    call    LightsChange            ;   then change mode
    btfsc   FLAGS, SECflag          ; Test SECOND flag
    call    SecondJobs              ;   and perform any work
    btfsc   FLAGS, TENflag          ; Test TEN SECOND flag
    call    TenSecondJobs           ;   and perform any work
    btfsc   FLAGS, MINflag          ; Test MINUTE flag
    call    MinuteJobs              ;   and perform any work
FeedLEDs:
    clrwdt
    movf    CAT1COLOR, WREG         ; initialize cat 1
    andlw   0x0F
    movwf   W1R
    swapf   CAT1COLOR, WREG
    andlw   0x0F
    movwf   W1G
    movf    CAT2COLOR, WREG         ; initialize cat 2
    andlw   0x0F
    movwf   W2R
    swapf   CAT2COLOR, WREG
    andlw   0x0F
    movwf   W2G
    movf    CAT3COLOR, WREG         ; initialize cat 3
    andlw   0x0F
    movwf   W3R
    swapf   CAT3COLOR, WREG
    andlw   0x0F
    movwf   W3G
    movf    CYLONLEVEL, WREG
    movwf   WCL
    movlw   0x10
    movwf   TMP
FeedLEDs_InnerLoop:
    movf    W1R, SELF           ; Cat 1 Red
    btfss   STATUS, ZEROFLAG
    decf    W1R, SELF
    btfss   STATUS, ZEROFLAG
    bsf     PORTB, Cat1Red
    movf    W1R, SELF
    btfsc   STATUS, ZEROFLAG
    bcf     PORTB, Cat1Red
    movf    W1G, SELF           ; Cat 1 Green
    btfss   STATUS, ZEROFLAG
    decf    W1G, SELF
    btfss   STATUS, ZEROFLAG
    bsf     PORTB, Cat1Green
    movf    W1G, SELF
    btfsc   STATUS, ZEROFLAG
    bcf     PORTB, Cat1Green
    movf    W2R, SELF           ; Cat 2 Red
    btfss   STATUS, ZEROFLAG
    decf    W2R, SELF
    btfss   STATUS, ZEROFLAG
    bsf     PORTB, Cat2Red
    movf    W2R, SELF
    btfsc   STATUS, ZEROFLAG
    bcf     PORTB, Cat2Red
    movf    W2G, SELF           ; Cat 2 Green
    btfss   STATUS, ZEROFLAG
    decf    W2G, SELF
    btfss   STATUS, ZEROFLAG
    bsf     PORTB, Cat2Green
    movf    W2G, SELF
    btfsc   STATUS, ZEROFLAG
    bcf     PORTB, Cat2Green
    movf    W3R, SELF           ; Cat 3 Red
    btfss   STATUS, ZEROFLAG
    decf    W3R, SELF
    btfss   STATUS, ZEROFLAG
    bsf     PORTB, Cat3Red
    movf    W3R, SELF
    btfsc   STATUS, ZEROFLAG
    bcf     PORTB, Cat3Red
    movf    W3G, SELF           ; Cat 3 Green
    btfss   STATUS, ZEROFLAG
    decf    W3G, SELF
    btfss   STATUS, ZEROFLAG
    bsf     PORTB, Cat3Green
    movf    W3G, SELF
    btfsc   STATUS, ZEROFLAG
    bcf     PORTB, Cat3Green
    movf    WCL, SELF           ; Cylon Level
    btfss   STATUS, ZEROFLAG
    decf    WCL, SELF
    btfss   STATUS, ZEROFLAG
    bcf     PORTA, OutEnable
    movf    WCL, SELF
    btfsc   STATUS, ZEROFLAG
    bsf     PORTA, OutEnable
    decfsz  TMP, SELF
    goto    FeedLEDs_InnerLoop
    goto    FeedLEDs

SubSecondJobs:
    call    IncTimeClock
    call    SetCylon
    bcf     INTCON, TIMER       ; clear timer flag
    btfsc   FLAGS, SECflag
    return
    retfie

SecondJobs:
    clrwdt
    bcf     FLAGS, SECflag
    btfss   PORTB, Sounder
    goto    SecondJobs_SounderOff
    btfsc   FLAGS, BEEPflag
    bcf     PORTB, Sounder
    bcf     FLAGS, BEEPflag
SecondJobs_SounderOff
    movf    LIGHTSON, SELF
    btfsc   STATUS, ZEROFLAG
    goto    Reset
    call    TestLights
    call    SetCylonLevel
    call    ModeLockout
    btfsc   FLAGS, TENflag
    return
    retfie

TenSecondJobs:
    clrwdt
    bcf     FLAGS, TENflag
    call    SetCatEyesX
    btfsc   FLAGS, MINflag
    return
    retfie

MinuteJobs:
    clrwdt
    movf    MINS, WREG
    xorlw   0x08                    ; Test for varying MINUTE values
    btfsc   STATUS, ZEROFLAG        ;   If they match then call LightsChange
    call    LightsChange            ;   This will force MODE to increment
    movf    MINS, WREG
    xorlw   0x10                    ; Switch MODE at 16 MINUTES
    btfsc   STATUS, ZEROFLAG
    call    LightsChange
    movf    MINS, WREG
    xorlw   0x18
    btfsc   STATUS, ZEROFLAG
    call    LightsChange
    bcf     FLAGS, MINflag
    retfie    

TestLights:
    movf    PORTB, SELF
    nop
    btfss   PORTB, LightsOff
    goto    TestLights_On
    movlw   0x10
    movwf   LIGHTSON
    return
TestLights_On:
    decf    LIGHTSON, SELF
    return
    
SetCatEyesX:
    clrwdt
    swapf   FSR, WREG
    movwf   FSRKEEPER
    movlw   0x03
    movwf   CAT_TMP
    movlw   CAT1COLOR
    movwf   FSR
SetCatEyesX_Red:
    movf    INDF, WREG
    andlw   0x0F
    movwf   BYTE1
    incf    FSR, SELF
    btfsc   INDF, RedSign           ; test direction
    goto    SetCatEyesX_DecRed      ; if NOT SET then
    movf    INDF, WREG              ; increment red
    andlw   0x03
    addwf   BYTE1, SELF
    btfss   STATUS, DIGITCARRY
    goto    SetCatEyesX_Green
    movlw   0x0F                    ; red above maximum
    iorwf   BYTE1, SELF             ; force red to full on
    btfsc   INDF, RedBounce         ; test red bounce mode
    bsf     INDF, RedSign           ; if SET then change sign
    goto    SetCatEyesX_Green
SetCatEyesX_DecRed:
    movf    INDF, WREG              ; increment red
    andlw   0x03
    subwf   BYTE1, SELF
    btfsc   STATUS, CARRYBIT
    goto    SetCatEyesX_Green
    movlw   0xF0                    ; red below minimum
    andwf   BYTE1, SELF             ; force red to off
    btfsc   INDF, RedBounce         ; test red bounce mode
    bcf     INDF, RedSign           ; if SET then change sign
SetCatEyesX_Green:
    decf    FSR, SELF
    swapf   INDF, WREG
    andlw   0x0F
    movwf   BYTE2
    incf    FSR, SELF
    btfsc   INDF, GrnSign           ; test direction
    goto    SetCatEyesX_DecGreen    ; if NOT SET then
    movf    INDF, WREG              ; increment red
    andlw   0x03
    addwf   BYTE2, SELF
    btfss   STATUS, DIGITCARRY
    goto    SetCatEyesX_SaveColors
    movlw   0x0F                    ; green above maximum
    iorwf   BYTE2, SELF             ; force red to full on
    btfsc   INDF, GrnBounce         ; test red bounce mode
    bsf     INDF, GrnSign           ; if SET then change sign
    goto    SetCatEyesX_SaveColors
SetCatEyesX_DecGreen:
    movf    INDF, WREG              ; increment red
    andlw   0x03
    subwf   BYTE2, SELF
    btfsc   STATUS, CARRYBIT
    goto    SetCatEyesX_SaveColors
    movlw   0xF0                    ; green below minimum
    andwf   BYTE2, SELF             ; force red to off
    btfsc   INDF, GrnBounce         ; test red bounce mode
    bcf     INDF, GrnSign           ; if SET then change sign
SetCatEyesX_SaveColors:
    movlw   0x00
    swapf   BYTE2, WREG
    iorwf   BYTE1, WREG
    decf    FSR, SELF
    movwf   INDF
SetCatEyesX_NextCat:
    incf    FSR, SELF
    incf    FSR, SELF
    decfsz  CAT_TMP, SELF
    goto    SetCatEyesX_Red
    swapf   FSRKEEPER, WREG
    movwf   FSR
    return

SetCylon:
    decfsz  WCS, SELF
    return
    clrwdt
    movf    CYLONSCALE, WREG
    movwf   WCS
    bcf     STATUS, CARRYBIT
    btfsc   FLAGS, CYLONcarry
    bsf     STATUS, CARRYBIT
    btfsc   CYLONCTL, 0x7
    goto    SetCylon_AllOn
    btfsc   CYLONCTL, 0x6
    goto    SetCylon_SpaceChase
    btfsc   CYLONCTL, 0x5
    goto    SetCylon_Done
    bcf     STATUS, CARRYBIT
    btfsc   CYLONCTL, 0x0
    goto    RotateRightA
    rlf     CYLONA, SELF
    btfss   CYLONA, 0x7
    goto    SetCylonB
    rrf     CYLONA, SELF
    rrf     CYLONA, SELF
    bsf     CYLONCTL, 0x0
    goto    SetCylonB
RotateRightA:
    rrf     CYLONA, SELF
    btfss   STATUS, CARRYBIT
    goto    SetCylonB
    rlf     CYLONA, SELF
    rlf     CYLONA, SELF
    bcf     CYLONCTL, 0x0
SetCylonB:
    btfsc   CYLONCTL, 0x1
    goto    RotateRightB
    rlf     CYLONB, SELF
    btfss   CYLONB, 0x7
    goto    SetCylon_Done
    rrf     CYLONB, SELF
    rrf     CYLONB, SELF
    bsf     CYLONCTL, 0x1
    goto    SetCylon_Done
RotateRightB:
    rrf     CYLONB, SELF
    btfss   STATUS, CARRYBIT
    goto    SetCylon_Done
    rlf     CYLONB, SELF
    rlf     CYLONB, SELF
    bcf     CYLONCTL, 0x1
    goto    SetCylon_Done
SetCylon_AllOn:
    movlw   0x7F
    movwf   CYLONA
    movwf   CYLONB
    goto    SetCylon_Done
SetCylon_SpaceChase:
    swapf   CYLONA, WREG
    xorlw   0xFF
    btfsc   STATUS, ZEROFLAG
    goto    SetCylon_SpaceChaseB
    rlf     CYLONA, SELF
    btfsc   CYLONA, 0x7
    goto    SetCylon_Done
    rlf     CYLONA, SELF
    rlf     CYLONB, SELF
    goto    SetCylon_Done
SetCylon_SpaceChaseB:
    rlf     CYLONB, SELF
    btfsc   CYLONB, 0x7
    goto    SetCylon_Done
    rlf     CYLONB, SELF
    rlf     CYLONA, SELF
SetCylon_Done:
    bcf     FLAGS, CYLONcarry
    btfsc   STATUS, CARRYBIT
    bsf     FLAGS, CYLONcarry
FeedCylon:
    clrwdt
    movlw   CYLONB
    movwf   FSR
    movlw   0x02
    movwf   TMP
FeedCylonLoop:
    movlw   0x08                ; bit counter
    movwf   TMP2
FeedCylonLoopBits:
    bcf     PORTA, SerialData
    nop
    rlf     INDF, SELF
    btfsc   STATUS, CARRYBIT
    bsf     PORTA, SerialData
    nop
    nop
    nop
    bsf     PORTA, Clock
    nop
    nop
    bcf     PORTA, Clock
    decfsz  TMP2, SELF
    goto    FeedCylonLoopBits
    rlf     INDF, SELF
    decf    FSR, SELF
    decfsz  TMP, SELF
    goto    FeedCylonLoop
    bcf     PORTA, SerialData
    bsf     PORTA, Latch
    nop
    nop
    nop
    bcf     PORTA, Latch
    return

SetCylonLevel:
    clrwdt
    btfsc   CYLONMODE, RedSign      ; test direction
    goto    SetCylonLevel_FadeOut   ; if NOT SET then
    movf    CYLONMODE, WREG         ; increment red
    andlw   0x03
    addwf   CYLONLEVEL, SELF
    btfss   STATUS, DIGITCARRY
    goto    SetCylonLevel_Done
    movlw   0x0F                    ; red above maximum
    iorwf   CYLONLEVEL, SELF        ; force red to full on
    btfsc   CYLONMODE, RedBounce    ; test red bounce mode
    bsf     CYLONMODE, RedSign      ; if SET then change sign
    goto    SetCylonLevel_Done
SetCylonLevel_FadeOut:
    movf    CYLONMODE, WREG
    andlw   0x03
    subwf   CYLONLEVEL, SELF
    btfsc   STATUS, CARRYBIT
    goto    SetCylonLevel_Done
    clrf    CYLONLEVEL              ; force LEDs to off
    btfsc   CYLONMODE, RedBounce    ; test red bounce mode
    bcf     CYLONMODE, RedSign      ; if SET then change sign
SetCylonLevel_Done:
    return

LightsChange:                       ; Called when the LIGHT level changes
    movf    PORTB, SELF
    nop
    btfss   PORTB, 0x0              ; 
    goto    LightChangeReturn
    clrwdt
    movf    MODE, SELF              ; test mode
    btfsc   STATUS, ZEROFLAG        ; if MODE=0
    goto    RunMode                 ;     then initial wake up
    movf    MODELOCK, SELF          ;     else test mode lockout
    btfsc   STATUS, ZEROFLAG        ;          if MODELOCK=0
    goto    ChangeMode              ;              then change mode
    goto    LightChangeReturn       ;              else ignore request
RunMode:
    movlw   0x1
    movwf   MODE
    call    RegReset
    call    Mode1Reset
    call    SetCylon
;    call    SetCatEyesX
    goto    LightChangeReturn
ChangeMode:
    movf    MODE, WREG              ; Test MODE for = 4 (overnight)
    xorlw   0x04                    ;   Don't allow MODE to increment if it 
    btfsc   STATUS, ZEROFLAG        ;   it is set to 4
    goto    LightChangeReturn
    movlw   0x02                    ; set mode lockout to two seconds
    movwf   MODELOCK
    incf    MODE, SELF
    bsf     FLAGS, BEEPflag
    bsf     PORTB, Sounder
    movf    MODE, WREG
    xorlw   0x02
    btfsc   STATUS, ZEROFLAG
    call    Mode2Reset
    movf    MODE, WREG
    xorlw   0x03
    btfsc   STATUS, ZEROFLAG
    call    Mode3Reset
    movf    MODE, WREG
    xorlw   0x04
    btfsc   STATUS, ZEROFLAG
    call    Mode4Reset
    movf    MODE, WREG
    xorlw   0x05
    btfss   STATUS, ZEROFLAG
    goto    LightChangeReturn
    clrf    MODE
    incf    MODE
    call    Mode1Reset
    call    SetCylon
LightChangeReturn:
    movlw   0x01
    movwf   WCS
    bcf     INTCON, INTERRUPT       ; clear interrupt flag
    retfie

IncTimeClock:
    clrwdt
    incf    SUBS, SELF
    movf    SUBS, WREG
    xorlw   0x04                    ; Number of sub-seconds per second
    btfss   STATUS, ZEROFLAG
    return
Secs:
    clrf    SUBS
    incf    SECS, SELF
    bsf     FLAGS, SECflag
    movf    SECS, WREG
    xorlw   0x0A
    btfss   STATUS, ZEROFLAG
    return
Tens:
    clrf    SECS
    incf    TENS, SELF
    bsf     FLAGS, TENflag
    movf    TENS, SELF
    xorlw   0x06
    btfss   STATUS, ZEROFLAG
    return
Mins:    
    clrf    TENS
    incf    MINS, SELF
    bsf     FLAGS, MINflag
    movf    MINS, WREG
    xorlw   0x3C
    btfss   STATUS, ZEROFLAG
    return
Hrs:    
    clrf    MINS
    incf    HOURS, SELF
    return

ModeLockout:
    movf    MODELOCK, SELF
    btfss   STATUS, ZEROFLAG
    decf    MODELOCK, SELF
    return

; ****************
; ** Initialize **
; ****************

Initialize:
    DISABLE_INTERRUPTS 
    goto    Initialize      ; (make sure)
    clrwdt
    bsf     STATUS,REGBANK  ; switch to bank one
    movlw   0x10            ; set PORTA directions
    movwf   TRISA
    movlw   0x01            ; set PORTB directions
    movwf   TRISB
    movlw   0xD7            ; set OPTION register
    movwf   OPTREG
    bcf     STATUS,REGBANK  ; switch to bank zero
    movlw   0x30            ; set INTERRUPT CONTROL register
    movwf   INTCON
    clrf    MODE            ; reset run mode to sleep
    movlw   0x08
    movwf   PORTA
    movlw   0x10
    movwf   LIGHTSON
    clrf    PORTB
    movlw   0x02            ; set mode lockout to two seconds
    movwf   MODELOCK
    movlw   0x01
    movwf   WCS
    clrf    CYLONA
    clrf    CYLONB
    retfie                  ; return and set global interrupt enable
    
RegReset:
    movlw   0x08
    movwf   PORTA           ; disable Cylon output
    bcf     PORTB, Sounder  ; turn sounder off
    clrf    SUBS            ; reset time clock
    clrf    SECS
    clrf    TENS
    clrf    MINS
    clrf    HOURS
    clrf    FLAGS
    return    

Mode1Reset:
    movlw   0x54
    movwf   PORTB           ; set cat eyes to green
    clrf    BYTE1
    clrf    BYTE2
    movlw   0x01
    movwf   CYLONA          ; set Cylon bank A LEDs to 1
    movwf   CYLONB          ; set Cylon bank B LEDs to 1
    movlw   B'11011001'     ; set cat eye colour change rates to
    movwf   CAT1MODE
    movlw   B'10101101'
    movwf   CAT2MODE
    movlw   B'10111111'
    movwf   CAT3MODE
    movlw   0x0D
    movwf   CYLONMODE       ; set Cylon to fade out and back continually
    movlw   0x0F
    movwf   CYLONLEVEL
    movlw   B'11110000'
    movwf   CAT1COLOR
    movlw   B'00001110'
    movwf   CAT2COLOR
    movlw   B'10001000'
    movwf   CAT3COLOR
    movlw   0x15
    clrf    CYLONCTL        ; set default Cylon operation
    movlw   0x02
    movwf   CYLONSCALE
    return

Mode2Reset:
    movlw   0x7F
    movwf   CYLONA
    movwf   CYLONB
    decf    CYLONA, SELF
    movlw   0x0D
    movwf   CYLONMODE
    movlw   0x08
    movwf   CYLONLEVEL
    clrf    CYLONCTL
    bsf     CYLONCTL, 0x7
    movlw   0xFF
    movwf   CYLONSCALE
    return

Mode3Reset:
    movlw   0xFF
    movwf   CYLONA
    movwf   CYLONB
    movlw   0x04
    movwf   CYLONSCALE
    decf    CYLONA, SELF
    movlw   0x00
    movwf   CYLONMODE       ; set Cylon to no fade
    movlw   0x0F
    movwf   CYLONLEVEL
    clrf    CYLONCTL
    bsf     CYLONCTL, 0x6   ; set Cylon to space chase
    bsf     STATUS, CARRYBIT
    bsf     FLAGS, CYLONcarry
    return
    
Mode4Reset:
    movlw   B'01111111'
    movwf   CYLONA
    movlw   B'01001001'
    movwf   CYLONB
    movlw   0x54
    movwf   PORTB           ; set cat eyes to green
    clrf    CAT1MODE        ; set cat eyes to not change
    clrf    CAT2MODE
    clrf    CAT3MODE
    clrf    CYLONMODE       ; set Cylon to no fade
    movlw   0x0A
    movwf   CYLONLEVEL
    movlw   B'11110000'     ; set all cats to have green eyes
    movwf   CAT1COLOR
    movwf   CAT2COLOR
    movwf   CAT3COLOR
    movlw   B'00100000'
    movwf   CYLONCTL        ; set Cylon to static
    movlw   0x0F
    movwf   CYLONSCALE
    return
    
    end
