; Radiator Cover Display
; Version 0.2
; May 2005
; (C) Copyright 1998-2005 Steven J Lilley
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation; either version 2 of the License, or
; (at your option) any later version.
; 
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
; GNU General Public License for more details.
; 
; You should have received a copy of the GNU General Public License
; along with this program; if not, write to the Free Software
; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA    02111-1307    USA
;

; Define processor/radix

    list    p=16F84, r=HEX
    errorlevel -302

; Define CONSTANTS
;

; **** System Registers ****
; (Bank 0)

    constant INDF=0x00      ; Indirect address reference, refers to register held in FSR
    constant STATUS=0x03    ; Status Register
    constant FSR=0x04       ; Indirect address pointer
    constant PORTA=0x05     ; Port A register
    constant PORTB=0x06     ; Port B register
    constant EEDATA=0x08    ; EEPROM data register
    constant EEADR=0x09     ; EEPROM address register
    constant INTCON=0x0B    ; Interrupt control register
    
; (Bank 1)

    constant OPTREG=0x81    ; Option register
    constant TRISA=0x85     ; Port A data direction register
    constant TRISB=0x86     ; Port B data direction register
    constant EECON1=0x88    ; EEPROM control register
    constant EECON2=0x89    ; EEPROM control register

; ****  User Registers  ****

; General purpose
TMP         equ     0x0C
TMP2        equ     0x0D
BYTE1       equ     0x0E
BYTE2       equ     0x0F

; Timer
SUBS        equ     0x10
SECS        equ     0x11
MINS        equ     0x12
HOURS       equ     0x13

; Control
FLAGS       equ     0x14
ENABLES     equ     0x15
MODE        equ     0x16
MODELOCK    equ     0x17

; Cylon
CYLONA      equ     0x20
CYLONB      equ     0x21
CYLONCTL    equ     0x22
CYLONLEVEL  equ     0x23
CYLONMODE   equ     0x24
CYLONRUNS   equ     0x25

; Cat Eyes
; ****  DO NOT ALTER REGISTER ORDER OF THESE SIX  ****
CAT1COLOR   equ     0x28
CAT1MODE    equ     0x29
CAT2COLOR   equ     0x2A
CAT2MODE    equ     0x2B
CAT3COLOR   equ     0x2C
CAT3MODE    equ     0x2D

; Fade control working registers
W1R         equ     0x30
W2R         equ     0x31
W3R         equ     0x32
W1G         equ     0x33
W2G         equ     0x34
W3G         equ     0x35
WCL         equ     0x36

; In interupt routines
BEEP1       equ     0x40
BEEP2       equ     0x41



; ****  Bit references  ****
SECflag     equ     0x0
MINflag     equ     0x1

; PORTA
Latch       equ     0x0
Clock       equ     0x1
SerialData  equ     0x2
OutEnable   equ     0x3

; STATUS register
REGBANK     equ     0x5     ; The bank indicator bit
WATCHDOG    equ     0x4     ; Watchdog timeout 
ZEROFLAG    equ     0x2     ; The ZERO flag bit
DIGITCARRY  equ     0x1     ; The DIGIT CARRY bit
CARRYBIT    equ     0x0     ; The CARRY bit

; OPTION register
PRESCALER   equ     0x3     ; Prescaler assignment

; INTCON register
GIE         equ     0x7     ; Global Interrupt Enable bit
EEIEN       equ     0x6
T0IEN       equ     0x5
INTEN       equ     0x4
RBIEN       equ     0x3
TIMER       equ     0x2
INTERRUPT   equ     0x1
RBIFLG      equ     0x0

; PORTB register
LightsOff   equ     0x0
Cat1Red     equ     0x1
Cat1Green   equ     0x2
Cat2Red     equ     0x3
Cat2Green   equ     0x4
Cat3Red     equ     0x5
Cat3Green   equ     0x6
Sounder     equ     0x7

; CATMODE register
Cat1IncGrn  equ     0x0
Cat1IncRed  equ     0x1
Cat2IncGrn  equ     0x2
Cat2IncRed  equ     0x3
Cat3IncGrn  equ     0x4
Cat3IncRed  equ     0x5

; CATxMODE register
RedRate1    equ     0x0
RedRate2    equ     0x1
RedSign     equ     0x2
RedBounce   equ     0x3
GrnRate1    equ     0x4
GrnRate2    equ     0x5
GrnSign     equ     0x6
GrnBounce   equ     0x7

; register references
WREG        equ     0x0
SELF        equ     0x1

; **** Macros ****

ENABLE_INTERRUPTS macro
    bsf     INTCON,GIE
    endm

DISABLE_INTERRUPTS macro
    bcf     INTCON,GIE
    btfsc   INTCON,GIE
    endm
    
SWITCH_TO_BANK0 macro
    bcf     STATUS,REGBANK
    endm

SWITCH_TO_BANK1 macro
    bsf     STATUS,REGBANK
    endm

; ***************
; **   START   **
; ***************
Reset:        org 0x000
    call    Initialize
Standby:
    sleep
    nop
Int:        org 0x004
    DISABLE_INTERRUPTS
    goto    Int
    btfss   STATUS, WATCHDOG        ; Watchdog
    goto    Standby
    btfsc   INTCON, INTERRUPT       ; Lights went out
    call    LightsOut
    btfsc   INTCON, TIMER           ; Timer overflowed
    call    SubSecondJobs
    btfsc   FLAGS, SECflag
    call    SecondJobs
    btfsc   FLAGS, MINflag
    call    MinuteJobs
Run:
    call    FeedCatEyes
    call    FeedCylon
    goto    Run

FeedCatEyes:
    clrwdt
    movf    CAT1COLOR, WREG         ; initialize cat 1
    andlw   0x0F
    movwf   W1R
    swapf   CAT1COLOR, WREG
    andlw   0x0F
    movwf   W1G
    movf    CAT2COLOR, WREG         ; initialize cat 2
    andlw   0x0F
    movwf   W2R
    swapf   CAT2COLOR, WREG
    andlw   0x0F
    movwf   W2G
    movf    CAT3COLOR, WREG         ; initialize cat 3
    andlw   0x0F
    movwf   W3R
    swapf   CAT3COLOR, WREG
    andlw   0x0F
    movwf   W3G
    movlw   0x08
    movwf   TMP
FeedCatEyesLoop:
    movf    W1R, SELF
    btfss   STATUS, ZEROFLAG
    decf    W1R, SELF
    btfss   STATUS, ZEROFLAG
    bsf     PORTB, Cat1Red
    movf    W1R, SELF
    btfsc   STATUS, ZEROFLAG
    bcf     PORTB, Cat1Red
    movf    W1G, SELF
    btfss   STATUS, ZEROFLAG
    decf    W1G, SELF
    btfss   STATUS, ZEROFLAG
    bsf     PORTB, Cat1Green
    movf    W1G, SELF
    btfsc   STATUS, ZEROFLAG
    bcf     PORTB, Cat1Green
    movf    W2R, SELF
    btfss   STATUS, ZEROFLAG
    decf    W2R, SELF
    btfss   STATUS, ZEROFLAG
    bsf     PORTB, Cat2Red
    movf    W2R, SELF
    btfsc   STATUS, ZEROFLAG
    bcf     PORTB, Cat2Red
    movf    W2G, SELF
    btfss   STATUS, ZEROFLAG
    decf    W2G, SELF
    btfss   STATUS, ZEROFLAG
    bsf     PORTB, Cat2Green
    movf    W2G, SELF
    btfsc   STATUS, ZEROFLAG
    bcf     PORTB, Cat2Green
    movf    W3R, SELF
    btfss   STATUS, ZEROFLAG
    decf    W3R, SELF
    btfss   STATUS, ZEROFLAG
    bsf     PORTB, Cat3Red
    movf    W3R, SELF
    btfsc   STATUS, ZEROFLAG
    bcf     PORTB, Cat3Red
    movf    W3G, SELF
    btfss   STATUS, ZEROFLAG
    decf    W3G, SELF
    btfss   STATUS, ZEROFLAG
    bsf     PORTB, Cat3Green
    movf    W3G, SELF
    btfsc   STATUS, ZEROFLAG
    bcf     PORTB, Cat3Green
    decfsz  TMP, SELF
    goto    FeedCatEyesLoop
    return

FeedCylon:
    clrwdt
    bsf     PORTA, OutEnable    ; turn off Cylon LEDs
    movlw   CYLONB
    movwf   FSR
    movlw   0x02
    movwf   TMP
FeedCylonLoop:
    movlw   0x08                ; bit counter
    movwf   TMP2
FeedCylonLoopBits:
    bcf     PORTA, SerialData
    nop
    rlf     INDF, SELF
    btfsc   STATUS, CARRYBIT
    bsf     PORTA, SerialData
    nop
    nop
    bsf     PORTA, Clock
    nop
;    nop
    bcf     PORTA, Clock
    decfsz  TMP2, SELF
    goto    FeedCylonLoopBits
    rlf     INDF, SELF
    decf    FSR, SELF
    decfsz  TMP, SELF
    goto    FeedCylonLoop
    bsf     PORTA, Latch
    nop
    nop
    bcf     PORTA, Latch
    decfsz  CYLONRUNS, SELF
    goto    FeedCylonFadeControl
    movlw   0x10
    movwf   CYLONRUNS
    movf    CYLONLEVEL, WREG
    movwf   WCL
    goto    FeedCylon_Done
FeedCylonFadeControl:
    movf    WCL, SELF
    btfss   STATUS, ZEROFLAG
    decf    WCL, SELF
    btfss   STATUS, ZEROFLAG
    bcf     PORTA, OutEnable
FeedCylon_Done:
    return

SubSecondJobs:
    clrwdt
    call    IncTimeClock
    call    SetCylon
    bcf     INTCON, TIMER       ; clear timer flag
    btfsc   FLAGS, SECflag
    return
    retfie

SecondJobs:
;    clrwdt
    bcf     FLAGS, SECflag
;    call    Beep
;    movf    PORTB, WREG         ; DEBUG to eval second timing
;    xorlw   0x80                ; DEBUG
;    movwf   PORTB               ; DEBUG
    call    SetCylonLevel
    call    ModeLockout
    btfsc   FLAGS, MINflag
    return
    retfie

MinuteJobs:
;    clrwdt
    bcf     FLAGS, MINflag
    call    SetCatEyesX
    retfie    

SetCatEyesX:
    movlw   0x03
    movwf   TMP
    movlw   CAT1COLOR
    movwf   FSR
SetCatEyesX_Red:
    movf    INDF, WREG
    andlw   0x0F
    movwf   BYTE1
    incf    FSR, SELF
    btfsc   INDF, RedSign           ; test direction
    goto    SetCatEyesX_DecRed      ; if NOT SET then
    movf    INDF, WREG              ; increment red
    andlw   0x03
    addwf   BYTE1, SELF
    btfss   STATUS, DIGITCARRY
    goto    SetCatEyesX_Green
    movlw   0x0F                    ; red above maximum
    iorwf   BYTE1, SELF             ; force red to full on
    btfsc   INDF, RedBounce         ; test red bounce mode
    bsf     INDF, RedSign           ; if SET then change sign
    goto    SetCatEyesX_Green
SetCatEyesX_DecRed:
    movf    INDF, WREG              ; increment red
    andlw   0x03
    subwf   BYTE1, SELF
    btfsc   STATUS, CARRYBIT
    goto    SetCatEyesX_Green
    movlw   0xF0                    ; red below minimum
    andwf   BYTE1, SELF             ; force red to off
    btfsc   INDF, RedBounce         ; test red bounce mode
    bcf     INDF, RedSign           ; if SET then change sign
SetCatEyesX_Green:
    decf    FSR, SELF
    swapf   INDF, WREG
    andlw   0x0F
    movwf   BYTE2
    incf    FSR, SELF
    btfsc   INDF, GrnSign           ; test direction
    goto    SetCatEyesX_DecGreen    ; if NOT SET then
    movf    INDF, WREG              ; increment red
    andlw   0x03
    addwf   BYTE2, SELF
    btfss   STATUS, DIGITCARRY
    goto    SetCatEyesX_SaveColors
    movlw   0x0F                    ; green above maximum
    iorwf   BYTE2, SELF             ; force red to full on
    btfsc   INDF, GrnBounce         ; test red bounce mode
    bsf     INDF, GrnSign           ; if SET then change sign
    goto    SetCatEyesX_SaveColors
SetCatEyesX_DecGreen:
    movf    INDF, WREG              ; increment red
    andlw   0x03
    subwf   BYTE2, SELF
    btfsc   STATUS, CARRYBIT
    goto    SetCatEyesX_SaveColors
    movlw   0xF0                    ; green below minimum
    andwf   BYTE2, SELF             ; force red to off
    btfsc   INDF, GrnBounce         ; test red bounce mode
    bcf     INDF, GrnSign           ; if SET then change sign
SetCatEyesX_SaveColors:
    movlw   0x00
    swapf   BYTE2, WREG
    iorwf   BYTE1, WREG
    decf    FSR, SELF
    movwf   INDF
SetCatEyesX_NextCat:
    incf    FSR, SELF
    incf    FSR, SELF
    decfsz  TMP, SELF
    goto    SetCatEyesX_Red
    return

SetCylon:
    btfsc   CYLONCTL, 0x7
    goto    SetCylon_Static
    btfsc   CYLONCTL, 0x6
    goto    SetCylon_SpaceChase
    bcf     STATUS, CARRYBIT
    btfsc   CYLONCTL, 0x0
    goto    RotateRightA
    rlf     CYLONA, SELF
    btfss   CYLONA, 0x7
    goto    SetCylonB
    rrf     CYLONA, SELF
    rrf     CYLONA, SELF
    bsf     CYLONCTL, 0x0
    goto    SetCylonB
RotateRightA:
    rrf     CYLONA, SELF
    btfss   STATUS, CARRYBIT
    goto    SetCylonB
    rlf     CYLONA, SELF
    rlf     CYLONA, SELF
    bcf     CYLONCTL, 0x0
SetCylonB:
    btfsc   CYLONCTL, 0x1
    goto    RotateRightB
    rlf     CYLONB, SELF
    btfss   CYLONB, 0x7
    goto    SetCylon_Done
    rrf     CYLONB, SELF
    rrf     CYLONB, SELF
    bsf     CYLONCTL, 0x1
    goto    SetCylon_Done
RotateRightB:
    rrf     CYLONB, SELF
    btfss   STATUS, CARRYBIT
    goto    SetCylon_Done
    rlf     CYLONB, SELF
    rlf     CYLONB, SELF
    bcf     CYLONCTL, 0x1
    goto    SetCylon_Done
SetCylon_Static:
    movlw   0x7F
    movwf   CYLONA
    movwf   CYLONB
    goto    SetCylon_Done
SetCylon_SpaceChase:
SetCylon_Done:
    return

SetCylonLevel:
    btfsc   CYLONMODE, RedSign      ; test direction
    goto    SetCylonLevel_FadeOut   ; if NOT SET then
    movf    CYLONMODE, WREG         ; increment red
    andlw   0x03
    addwf   CYLONLEVEL, SELF
    btfss   STATUS, DIGITCARRY
    goto    SetCylonLevel_Done
    movlw   0x0F                    ; red above maximum
    iorwf   CYLONLEVEL, SELF        ; force red to full on
    btfsc   CYLONMODE, RedBounce    ; test red bounce mode
    bsf     CYLONMODE, RedSign      ; if SET then change sign
    goto    SetCylonLevel_Done
SetCylonLevel_FadeOut:
    movf    CYLONMODE, WREG
    andlw   0x03
    subwf   CYLONLEVEL, SELF
    btfsc   STATUS, CARRYBIT
    goto    SetCylonLevel_Done
    clrf    CYLONLEVEL              ; force LEDs to off
    btfsc   CYLONMODE, RedBounce    ; test red bounce mode
    bcf     CYLONMODE, RedSign      ; if SET then change sign
SetCylonLevel_Done:
    return

LightsOut:
    movf    MODE, SELF              ; test mode
    btfsc   STATUS, ZEROFLAG        ; if MODE=0
    goto    RunMode                 ;     then initial wake up
    movf    MODELOCK, SELF          ;     else test mode lockout
    btfsc   STATUS, ZEROFLAG        ;          if MODELOCK=0
    goto    ChangeMode              ;              then change mode
    goto    LightChangeReturn       ;              else ignore request
RunMode:
    movlw   0x1
    movwf   MODE
    call    RegReset
    goto    LightChangeReturn
ChangeMode:
    rlf     MODE, SELF
    call    Beep
LightChangeReturn:
    bcf     INTCON, INTERRUPT       ; clear interrupt flag
    retfie

IncTimeClock:
    incf    SUBS, SELF
    movf    SUBS, WREG
    xorlw   0x0a
    btfss   STATUS, ZEROFLAG
    return
Secs:
    clrf    SUBS
    incf    SECS, SELF
    bsf     FLAGS, SECflag
    movf    SECS, WREG
    xorlw   0x3c
    btfss   STATUS, ZEROFLAG
    return
Mins:    
    clrf    SECS
    incf    MINS, SELF
    bsf     FLAGS, MINflag
    movf    MINS, WREG
    xorlw   0x3C
    btfss   STATUS, ZEROFLAG
    return
Hrs:    
    clrf    MINS
    incf    HOURS, SELF
    return

ModeLockout:
    movf    MODELOCK, SELF
    btfss   STATUS, ZEROFLAG
    decf    MODELOCK, SELF
    return

Beep:
    clrwdt
    movlw   0x10
    movwf   BEEP2
BeepOuterLoop:
    movlw   0xFF
    movwf   BEEP1
BeepInnerLoop:
    bcf     PORTB, Sounder
    nop
    nop
    bsf     PORTB, Sounder
    nop
    nop
    decfsz  BEEP1, SELF
    goto    BeepInnerLoop
    decfsz  BEEP2, SELF
    goto    BeepOuterLoop
    return

; ****************
; ** Initialize **
; ****************

Initialize:
    DISABLE_INTERRUPTS 
    goto    Initialize      ; (make sure)
    SWITCH_TO_BANK1
    movlw   0xD4            ; set OPTION register
    movwf   OPTREG
    movlw   0x10            ; set PORTA directions
    movwf   TRISA
    movlw   0x01            ; set PORTB directions
    movwf   TRISB
    SWITCH_TO_BANK0
    movlw   0x30            ; set INTERRUPT CONTROL register
    movwf   INTCON
    clrf    MODE            ; reset run mode to sleep
    movlw   0x08
    movwf   PORTA
    clrf    PORTB
    movlw   0x02            ; set mode lockout to two seconds
    movwf   MODELOCK
    retfie                  ; return and set global interrupt enable
    
RegReset:
    clrf    PORTA           ; set PORTA to low
    movlw   0x53
    movwf   PORTB           ; set cat eyes to green
    clrf    BYTE1
    clrf    BYTE2
    clrf    SUBS            ; reset time clock
    clrf    SECS
    clrf    MINS
    clrf    HOURS
    clrf    FLAGS
    movlw   0x01
    movwf   CYLONA          ; set Cylon bank A LEDs to 1
    movwf   CYLONB          ; set Cylon bank B LEDs to 1
    movwf   CAT1MODE        ; set cat eye colour change rates to
    movwf   CAT2MODE        ; green=0 and red=1
    movwf   CAT3MODE
    incf    CAT2MODE, SELF  ; XXXXXXXXXXXXXXXX TEST XXXXXXXXXXXXXXXXXXXX
    incf    CAT2MODE, SELF  ; XXXXXXXXXXXXXXXX TEST XXXXXXXXXXXXXXXXXXXX
    movlw   0x0D
    movwf   CYLONMODE       ; set Cylon to fade out and back continually
    movlw   0x0F
    movwf   CYLONLEVEL
    movlw   0x10
    movwf   CYLONRUNS
    movlw   0xF0
    movwf   CAT1COLOR
    movwf   CAT2COLOR
    movwf   CAT3COLOR
    movlw   0x15
    clrf    CYLONCTL        ; set default Cylon operation
    bsf     CYLONCTL, 0x7
    return    

    end
